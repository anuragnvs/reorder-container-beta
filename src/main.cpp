#include <iostream>
#include <vector>
#include <array>
#include <deque>
#include <forward_list>
#include <list>
#include <stdio.h>

#include "reordercontainer.h"
using namespace std;

void myFun(st_data& _data)
{
    cout<<" in myFun "<<" x "<<+_data.x<<" y "<<_data.y<<endl;
   _data.x +=200;
   _data.y +=100;
   return;
}

int main() {
		
   if (__cplusplus == 201703L)
      std::cout << "C++17" << endl;
   else if (__cplusplus == 201402L)
      std::cout << "C++14" << endl;
   else if (__cplusplus == 201103L)
      std::cout << "C++11" << endl;
   else if (__cplusplus == 199711L)
      std::cout << "C++98" << endl;
   else
      std::cout << "pre-standard C++" << endl;

   //for testing
   st_data d1(1,1.0);
   st_data d2(2,2.0);
   st_data d3(3,3.0);
   st_data d4(4,4.0);
   st_data d5(8,5.0);
   st_data d6(10,7.0);
   st_data d7(12,6.0);

   //vector<st_data> _container {d1,d2,d3,d4,d5,d6,d7};
//   cout<<"vector "<<_containerV.size()<<endl;
//   list<st_data> _containerL {d1,d2,d3,d4,d5,d6,d7};
//   cout<<"list "<<_containerL.size()<<endl;
  forward_list<st_data> _container {d1,d2,d3,d4,d5,d6,d7};
//   //cout<<"forward_list "<<_containerFL.size()<<endl;
//   deque<st_data> _containerDQ {d1,d2,d3,d4,d5,d6,d7};
//   cout<<"deque "<<_containerDQ.size()<<endl;
   for(auto item: _container) cout<<" (" <<+item.x<<", "<<item.y<<" )"<<endl ;
   cout<<endl;
   auto l = [](){cout<<"********** ";};
   reorderContainerABCCallback(_container, 4, 2,[](st_data& _data){
      _data.x+=200;
      _data.y+=200;
   });
   

   for(auto item: _container) cout<<" (" <<+item.x<<", "<<item.y<<" )"<<endl ;
   cout<<endl;

   return 0;
}
